import Objects.LearnList;
import Objects.ParseObjectId;
import Objects.UserSettings;
import Scheduler.DumbLearnScheduler;
import Scheduler.LearnProfile.LearnProfile;
import Scheduler.LearnProfile.SimpleFullLearnProfile;
import Scheduler.LearnScheduler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

/**
 */
public class Main
{
    public static final int STUDY_DAYS = 18;
    public static final int WORD_COUNT = 128;
    public static int nextId = 0;

    public static void main(String[] args)
    {
        // Generating simplified "dictionary"
        List<ParseObjectId> pairs = new ArrayList<>();
        for (int i = 0; i < WORD_COUNT; i++)
            pairs.add(randomId());

        // Generating simplified learning list containing all dictionary items.
        LearnList list = new LearnList();
        list.pairs = pairs;
        list.objectId = randomId();

        // Generating simplified learning profile
        LearnProfile profile = SimpleFullLearnProfile.build(randomId(), randomId(), "de", "en");

        UserSettings settings = new UserSettings();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, STUDY_DAYS - 1);

        LearnScheduler scheduler = DumbLearnScheduler.build(settings, profile, list, cal.getTime());





    }

    private static ParseObjectId randomId()
    {
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder sb = new StringBuilder();
        Random r = new Random();

        for (int i = 0; i < 10; i++)
            sb.append(chars.charAt(r.nextInt(chars.length())));

        return ParseObjectId.Of(sb.toString());
    }
    
    private static ParseObjectId generateSequentialId()
    {
        String id = Integer.toString(nextId++);
        while (id.length() < 10)
            id = "0" + id;
        
        return ParseObjectId.Of(id);
    }
}
