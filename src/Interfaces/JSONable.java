package Interfaces;

import org.json.JSONException;
import org.json.JSONObject;

/**
 */
public interface JSONable
{
    /**
     * Creates a JSON representation of this object.  This object should be able to be reconstructed from the generated
     * object using this object's .fromJSON() method.
     *
     * @return A JSON object representing this object.
     * @throws JSONException
     */
    JSONObject toJSON() throws JSONException;
}
