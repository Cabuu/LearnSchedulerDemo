package Objects;

import java.util.regex.Pattern;

public class ParseObjectId
{
    private static final Pattern PATTERN = Pattern.compile("^[a-zA-Z\\d]{10}$");

    public final String id;

    private ParseObjectId(String id)
    {
        if (id != null && id.equals(""))
            this.id = null;
        else if (id == null || PATTERN.matcher(id).matches())
            this.id = id;
        else
            throw new RuntimeException("Invalid Parse object ID.  Given '" + id + "'.");
    }

    public static ParseObjectId Of(String id)
    {
        return new ParseObjectId(id);
    }

    public static ParseObjectId empty()
    {
        return new ParseObjectId(null);
    }

    public boolean exists()
    {
        return id != null;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParseObjectId that = (ParseObjectId) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode()
    {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString()
    {
        return id;
    }
}
