package Objects;

import Interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * GREATLY SIMPLIFIED version of a LearnList class, used here just as a placeholder
 */
public class LearnList implements JSONable
{
    public ParseObjectId objectId;
    public List<ParseObjectId> pairs = new ArrayList<>();

    @Override
    public JSONObject toJSON() throws JSONException
    {
        JSONObject obj = new JSONObject();

        obj.put("objectId", objectId.id);

        return obj;
    }

    public static LearnList buildFromId(ParseObjectId objectId)
    {
        // This is where a learning list would be constructed either by finding the learning list with the given
        // object ID in the local database or by querying for it remotely.
        return new LearnList();
    }
}
