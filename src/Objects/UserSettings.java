package Objects;

import Interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * GREATLY SIMPLIFIED version of a UserSettings class, used here just as a placeholder
 */
public class UserSettings implements JSONable
{
    @Override
    public JSONObject toJSON() throws JSONException
    {
        return new JSONObject();
    }

    public static UserSettings fromJSON(JSONObject obj)
    {
        return new UserSettings();
    }
}
