package Objects;

/**
 * GREATLY SIMPLIFIED version of a TranslationPair class, used here just as a placeholder.  Hashed on an ID value.
 */
public class TranslationPair
{
    public final ParseObjectId id;

    /**
     * Private constructor, use static constructor instead.
     */
    private TranslationPair(ParseObjectId id)
    {
        this.id = id;
    }

    /**
     * Static constructor, creates a new pairId object and returns it.
     *
     * @return The newly constructed TranslationPair object.
     */
    public static TranslationPair buildFromId(ParseObjectId id)
    {
        return new TranslationPair(id);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TranslationPair pair = (TranslationPair) o;

        return id.equals(pair.id);

    }

    @Override
    public int hashCode()
    {
        return id.hashCode();
    }
}
