package Utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 */
public class Vals
{
    // Date formats.  The timezones of these formats are set to UTC inside the constructor
    public static final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final SimpleDateFormat ISO_DATE =
            new SimpleDateFormat(ISO_DATE_FORMAT, Locale.GERMANY);

    public enum Lang
    {
        en,
        de,
        es,
        fr,
        la,
        it,
        ru
    }
}
