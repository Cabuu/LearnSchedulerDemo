package Scheduler.LearnEvent;

import Interfaces.JSONable;
import Objects.ParseObjectId;
import Scheduler.EncounterHistory.EncounterHistory;
import Scheduler.EncounterHistory.HistoryCollection;
import Scheduler.EncounterHistory.LearnEncounter;
import Scheduler.Enums;
import Scheduler.LearnProfile.LearnProfile;
import Utils.Vals;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.stream.Collectors;

/**
 */
public abstract class LearnEvent implements JSONable
{    
    protected static final String FIELD_ID = "id";
    protected static final String FIELD_TYPE = "type";
    protected static final String FIELD_DATE = "date";
    protected static final String FIELD_STATUS = "status";
    protected static final String FIELD_HISTORY = "history";
    protected static final String FIELD_POSITION = "position";
    protected static final String FIELD_PRESENTATIONS = "presentations";
    
    public static int nextId = 0;
    
    public final int id;
    public final Enums.EventType type;
    public final Date date;
    public Enums.EventStatus status;
    protected final Set<ParseObjectId> pairs = new HashSet<ParseObjectId>();
    protected final LearnProfile profile;
    protected List<LearnPresentation> presentations = new ArrayList<>();
    protected int position;
    protected HistoryCollection encounters = HistoryCollection.build(this::buildEmptyHistory);

    
    
    /**
     * Private constructor, use static constructor instead.
     */
    protected LearnEvent(int id, Enums.EventType type, Date date, Enums.EventStatus status, LearnProfile profile,
                         Collection<ParseObjectId> pairs, int position, Collection<LearnPresentation> presentations,
                         HistoryCollection history)
    {
        this.id = id;
        this.type = type;
        this.date = date;
        this.status = status;
        this.profile = profile;
        this.position = position;
        if (presentations != null) this.presentations.addAll(presentations);
        if (history != null) this.encounters.mergeCollection(history);

        if (pairs != null) setPairs(pairs);
    }

    /**
     * Obviously there can be a more robust method of generating event IDs, but this was simple enough to write
     * quickly and gets the job done.
     */
    protected static int generateId()
    {
        return nextId++;
    }

    
    @Override
    public JSONObject toJSON() throws JSONException
    {
        JSONObject obj = new JSONObject();
        
        obj.put(FIELD_ID, id);
        obj.put(FIELD_TYPE, type.name());
        obj.put(FIELD_DATE, Vals.ISO_DATE.format(date));
        obj.put(FIELD_STATUS, status.name());
        
        JSONObject history = new JSONObject();
        for (EncounterHistory entry : encounters.getAllEncounterHistories())
            history.put(entry.pairId.id, entry.toJSON());
        history.put(FIELD_HISTORY, history);
        
        obj.put(FIELD_HISTORY, history);
        
        return obj;
    }
    
    public void addEncounter(LearnEncounter encounter)
    {
        encounters.addEncounter(encounter);
    }
    public void addEncounters(Collection<LearnEncounter> encounters)
    {
        encounters.forEach(this::addEncounter);
    }
    
    public void addHistory(EncounterHistory history)
    {
        encounters.mergeHistory(history);
    }

    public LearnPresentation getPresentation(int pos)
    {
        return pos >= 0 && pos < presentations.size() ? presentations.get(pos) : null;
    }

    public LearnPresentation getCurrentPresentation()
    {
        return getPresentation(position);
    }

    public int getPosition()
    {
        return position;
    }

    public Collection<LearnPresentation> getAllPresentations()
    {
        return presentations;
    }

    public int getPresentationCount()
    {
        return presentations.size();
    }

    /*
     * Managing event progression
     */

    public LearnPresentation peekNext()
    {
        recalculateOrdering(position + 1);
        return getPresentation(position + 1);
    }

    public LearnPresentation next()
    {
        position++;
        recalculateOrdering();
        return getCurrentPresentation();
    }

    public boolean isStarted()
    {
        return position >= 0;
    }

    public boolean isFinished()
    {
        return position >= presentations.size();
    }

    /*
     * Modifying word list
     */

    public void addPair(ParseObjectId id)
    {
        pairs.add(id);
        encounters.addTranslationPair(id);
    }

    public void addPairs(Collection<ParseObjectId> ids)
    {
        ids.forEach(this::addPair);
    }

    public void removePair(ParseObjectId id)
    {
        pairs.remove(id);
        encounters.removeTranslationPair(id);
    }

    public void removePairs(Collection<ParseObjectId> ids)
    {
        ids.forEach(this::removePair);
    }

    public void setPairs(Collection<ParseObjectId> pairs)
    {
        this.pairs.clear();
        this.pairs.addAll(pairs);

        HistoryCollection coll = HistoryCollection.build(this::buildEmptyHistory);
        for (ParseObjectId id : pairs)
        {
            if (encounters.hasTranslationPair(id))
                coll.mergeHistory(encounters.getHistory(id));
            else
                coll.addTranslationPair(id);
        }
        encounters = coll;
    }

    /**
     * This function returns a list in order to allow subclasses to override this and provide an ordering to the pairs.
     * By default, though, no order is applied due to converting directly from a set.
     */
    public List<ParseObjectId> getPairs()
    {
        return new ArrayList<>(pairs);
    }

    public Enums.EventStatus autoUpdateStatus()
    {
        long time = new Date().getTime();
        Date today = new Date(time - time % (24 * 60 * 60 * 1000));
        if (isFinished())
            status = Enums.EventStatus.COMPLETE;
        else if (isStarted())
            status = Enums.EventStatus.INCOMPLETE;
        else if (today.before(today))
            status = Enums.EventStatus.MISSED;
        else
            status = Enums.EventStatus.PENDING;

        return status;
    }


    /*
     * STATIC METHODS
     */

    protected static Collection<ParseObjectId> getPairsFromPresentations(Collection<LearnPresentation> presentations)
    {
        return presentations.stream()
                            .map(p -> p.pairId)
                            .collect(Collectors.toSet());
    }

    /*
     * ABSTRACT METHODS
     */
    
    protected abstract EncounterHistory buildEmptyHistory(ParseObjectId pair);

    public abstract LearnEvent recalculateOrdering(int position);

    public LearnEvent recalculateOrdering()
    {
        return recalculateOrdering();
    }

    public abstract void addEncounter(LearnPresentation presentation, Enums.EncounterResult result);

    
    
}
