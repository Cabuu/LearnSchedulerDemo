package Scheduler.LearnEvent;

import Objects.ParseObjectId;
import Scheduler.EncounterHistory.DumbEncounterHistory;
import Scheduler.EncounterHistory.EncounterHistory;
import Scheduler.EncounterHistory.HistoryCollection;
import Scheduler.EncounterHistory.LearnEncounter;
import Scheduler.Enums;
import Scheduler.LearnProfile.LearnProfile;
import Utils.Vals;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A completely dumbed down learning event that uses the DumbEncounterHistory and DumbLearnProfile
 */
public class DumbLearnEvent extends LearnEvent
{
    protected DumbLearnEvent(int id, Enums.EventType type, Date date, Enums.EventStatus status, LearnProfile profile,
                             Collection<ParseObjectId> pairs, int position, Collection<LearnPresentation> presentations,
                             HistoryCollection history)
    {
        super(id, type, date, status, profile, pairs, position, presentations, history);
    }

    public static LearnEvent build(int id, Enums.EventType type, Date date, Enums.EventStatus status, LearnProfile profile,
                                   Collection<ParseObjectId> pairs, int position, Collection<LearnPresentation> presentations,
                                   HistoryCollection history)
    {
        return new DumbLearnEvent(id, type, date, status, profile, pairs, position, presentations, history);
    }

    public static LearnEvent buildNew(Enums.EventType type, Date date, LearnProfile profile, Collection<ParseObjectId> pairs)
    {
        return build(generateId(),
                     type,
                     date,
                     Enums.EventStatus.PENDING,
                     profile,
                     pairs,
                     -1,
                     Collections.emptyList(),
                     null);
    }

    public static LearnEvent fromJSON(LearnProfile profile, JSONObject obj) throws JSONException, ParseException
    {
        JSONObject histObj = obj.getJSONObject(FIELD_HISTORY);

        // wtf is this in java, I can't just give a method reference if it has a 'throws' clause?  ugh
        HistoryCollection hist = HistoryCollection.fromJSON(DumbEncounterHistory::build,
                                                            (obj1) -> {
                                                                try
                                                                {
                                                                    return DumbEncounterHistory.fromJSON(obj1);
                                                                } catch (JSONException e)
                                                                {
                                                                    return null;
                                                                }
                                                            },
                                                            histObj);

        JSONArray presObj = obj.getJSONArray(FIELD_PRESENTATIONS);
        List<LearnPresentation> presentations = new ArrayList<>();
        for (int i = 0; i < presObj.length(); i++)
            presentations.add(LearnPresentation.fromJSON(presObj.getJSONObject(i)));

        return build(obj.getInt(FIELD_ID),
                     Enums.EventType.valueOf(obj.getString(FIELD_TYPE)),
                     Vals.ISO_DATE.parse(obj.getString(FIELD_DATE)),
                     Enums.EventStatus.valueOf(obj.getString(FIELD_STATUS)),
                     profile,
                     getPairsFromPresentations(presentations),
                     obj.getInt(FIELD_POSITION),
                     presentations,
                     hist);
    }

    /*
     * OVERRIDES/IMPLEMENTS
     */


    @Override
    public LearnPresentation next()
    {
        // For this subclass, don't recalculate ordering
        position++;
        return getCurrentPresentation();
    }

    @Override
    public LearnPresentation peekNext()
    {
        // For this subclass, don't recalculate ordering
        return getPresentation(position + 1);
    }


    @Override
    public LearnEvent recalculateOrdering(int position)
    {
        presentations.subList(position, presentations.size()).clear();

        Set<ParseObjectId> alreadyPresented = new HashSet<ParseObjectId>(encounters.getAllTranslationPairIds());
        List<ParseObjectId> remaining = pairs.stream()
                                             .filter(x -> !alreadyPresented.contains(x))
                                             .collect(Collectors.toList());

        Enums.EncounterType et;
        switch (type)
        {
            case LEARN:
                et = Enums.EncounterType.Study_Default;
                break;
            case EXAM:
            case TEST:
                et = Enums.EncounterType.Test_LetterSalad;
                break;
            default:
                et = Enums.EncounterType.Test_LetterSalad;
                break;
        }

        remaining.forEach(id -> presentations.add(LearnPresentation.build(id, et)));

        return this;
    }

    @Override
    public void addEncounter(LearnPresentation presentation, Enums.EncounterResult result)
    {
        LearnEncounter enc = LearnEncounter.build(presentation.pairId,
                                                  presentation.type,
                                                  result,
                                                  new Date());

        addEncounter(enc);
    }

    @Override
    protected EncounterHistory buildEmptyHistory(ParseObjectId pairId)
    {
        return DumbEncounterHistory.build(pairId);
    }
}
