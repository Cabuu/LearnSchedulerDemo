package Scheduler.LearnEvent;

import Objects.ParseObjectId;
import Scheduler.Enums;
import org.json.JSONException;
import org.json.JSONObject;

public class LearnPresentation
{

    // CONSTANTS
    protected static final String FIELD_PAIR_ID = "pairId";
    protected static final String FIELD_TYPE = "type";

    public final ParseObjectId pairId;
    public final Enums.EncounterType type;

    private LearnPresentation(ParseObjectId pairId, Enums.EncounterType type)
    {
        this.pairId = pairId;
        this.type = type;
    }

    public static LearnPresentation build(ParseObjectId pairId, Enums.EncounterType type)
    {
        return new LearnPresentation(pairId, type);
    }

    public static LearnPresentation fromJSON(JSONObject obj) throws JSONException
    {
        return build(ParseObjectId.Of(obj.getString(FIELD_PAIR_ID)),
                                     Enums.EncounterType.valueOf(obj.getString(FIELD_TYPE)));

    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject obj = new JSONObject();

        obj.put(FIELD_PAIR_ID, pairId.id);
        obj.put(FIELD_TYPE, type.name());

        return obj;
    }
}
