package Scheduler.EncounterHistory;

import Interfaces.JSONable;
import Objects.ParseObjectId;
import Scheduler.Enums;
import Utils.Vals;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;

/**
 * Class for representing a user's encounter with a particular pairId pairId during an exercise.
 */
public class LearnEncounter implements JSONable
{
    private static final String FIELD_PAIR_ID = "pairId";
    private static final String FIELD_EXERCISE_TYPE = "exerciseType";
    private static final String FIELD_RESULT = "result";
    private static final String FIELD_TIMESTAMP = "timestamp";

    public ParseObjectId pairId;
    public Enums.EncounterType exerciseType;
    public Enums.EncounterResult result;
    public Date timestamp;


    /**
     * Private constructor, use static constructor instead.
     */
    private LearnEncounter(ParseObjectId pairId, Enums.EncounterType type, Enums.EncounterResult result, Date time)
    {
        this.pairId = pairId;
        this.exerciseType = type;
        this.result = result;
        this.timestamp = time;
    }

    /**
     * Static constructor, creates a new learn encounter object and returns it.
     *
     * @return The newly constructed LearnEncounter object.
     */
    public static LearnEncounter build(ParseObjectId pairId, Enums.EncounterType type, Enums.EncounterResult result, Date time)
    {
        return new LearnEncounter(pairId, type, result, time);
    }

    public static LearnEncounter fromJSON(JSONObject obj) throws JSONException, ParseException
    {
        return build(ParseObjectId.Of(obj.getString(FIELD_PAIR_ID)),
                     Enums.EncounterType.valueOf(obj.getString(FIELD_EXERCISE_TYPE)),
                     Enums.EncounterResult.valueOf(obj.getString(FIELD_RESULT)),
                     Vals.ISO_DATE.parse(obj.getString(FIELD_TIMESTAMP)));
    }

    @Override
    public JSONObject toJSON() throws JSONException
    {
        JSONObject obj = new JSONObject();

        obj.put(FIELD_PAIR_ID, pairId.id);
        obj.put(FIELD_EXERCISE_TYPE, exerciseType.name());
        obj.put(FIELD_RESULT, result.name());
        obj.put(FIELD_TIMESTAMP, Vals.ISO_DATE.format(timestamp));

        return obj;
    }


}
