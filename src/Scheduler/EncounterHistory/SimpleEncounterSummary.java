package Scheduler.EncounterHistory;

import Objects.ParseObjectId;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Collection;

/**
 * This subclass of EncounterHistory creates a summary of all encounters, rather than keeping the full history in
 * memory/storage.  This has the advantage of taking up less space both on the device and in the remote database.
 * However, since the encounter data is lost, no new calculations may be done on the data retroactively.
 *
 * The encounter valuation is based on the "SimpleEncounterHistory" model, where correct answers count as a value of
 * 1.0, and all other results are 0.0.  A simple average of all encounter results is taken as the overall competency
 * level.
 */
public class SimpleEncounterSummary extends SimpleEncounterHistory
{
    private float competency = 0f;
    private int numEncounters = 0;

    /**
     * Private constructor, use static constructor instead.
     */
    private SimpleEncounterSummary(ParseObjectId pair, Collection<LearnEncounter> encounters,
                                   float competency, int numEncounters)
    {
        super(pair, encounters);
        this.competency = competency;
        this.numEncounters = numEncounters;
        getCompetency();
    }

    /**
     * Static constructor, creates a new simple full encounter history object and returns it.
     *
     * @return The newly constructed SimpleFullEncounterHistory object.
     */
    public static EncounterHistory build(ParseObjectId pair, Collection<LearnEncounter> encounters,
                                         float competency, int numEncounters)
    {
        return new SimpleEncounterSummary(pair, encounters, competency, numEncounters);
    }

    public static EncounterHistory fromJSON(JSONObject obj) throws JSONException, ParseException
    {
        return build(ParseObjectId.Of(obj.getString(FIELD_PAIR_ID)),
                     extractEncounters(obj),
                     (float) obj.getDouble(FIELD_COMPETENCY),
                     obj.getInt(FIELD_NUM_ENCOUNTERS));
    }

    /**
     * {@inheritDoc}
     *
     * This method calculates a competency value based on the previously cached competency value and any new
     * encounters that have been added since the last calculation.  If there have been no new encounters, this simply
     * returns the cached competency value.
     *
     * An average is calculated between the cached value and the value of the new encounters, with each value weighted
     * by the number of encounters it represents.
     */
    @Override
    public float getCompetency()
    {
        // If there are no new encounters to calculate for, return the cached competency value.
        if (encounters.size() == 0) return competency;

        // Collect the sum total (NOT the average) of the individual encounter results
        // This is because if we divide by the encounter count to get the average, we're just going to have to multiply
        // it by that count again to get the weighted value... so this simply cuts out the redundancy.
        float val = (float) encounters.stream()
                                      .mapToDouble(this::encounterValue)
                                      .sum();

        // Get the weighted average of the old competency value and the new one, with each value weighted by the number
        // of encounters
        competency = ((competency * numEncounters) + val) / (numEncounters + encounters.size());

        // Increase the encounter counter
        numEncounters += encounters.size();

        // Clear all encounters from the list
        encounters.clear();

        return competency;
    }

    /**
     * Overrides the superclass's toJSON() method, ensuring that all encounters are cleared before encoding.
     */
    @Override
    public JSONObject toJSON() throws JSONException
    {
        getCompetency();  // ensures encounter list is emptied and final competency value is cached.
        return super.toJSON();
    }

    /**
     * Returns the number of encounters in the encounter history.  This may simply
     * return encounters.size() for subclasses that use a full encounter history,
     * or some cached value for subclasses that use an encounter summary.
     */
    @Override
    protected int getNumOfEncounters()
    {
        return numEncounters;
    }

    @Override
    protected void sortEncounters()
    {
        // No need to sort the encounters, so overriding this method to do nothing.
    }
}
