package Scheduler.EncounterHistory;

import Objects.ParseObjectId;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Collection;

/**
 * This subclass of EncounterHistory keeps a full record of all learning encounters.  Whenever the getCompetency()
 * method is called, a new competency value is calculated based on all stored encounters.
 *
 * The encounter valuation is based on the "SimpleEncounterHistory" model, where correct answers count as a value of
 * 1.0, and all other results are 0.0.  A simple average of all encounter results is taken as the overall competency
 * level.
 */
public class SimpleFullEncounterHistory extends SimpleEncounterHistory
{
    /**
     * Private constructor, use static constructor instead.
     */
    private SimpleFullEncounterHistory(ParseObjectId pair, Collection<LearnEncounter> encounters)
    {
        super(pair, encounters);
    }

    /**
     * Static constructor, creates a new simple full encounter history object and returns it.
     *
     * @return The newly constructed SimpleFullEncounterHistory object.
     */
    public static EncounterHistory build(ParseObjectId pair, Collection<LearnEncounter> encounters)
    {
        return new SimpleFullEncounterHistory(pair, encounters);
    }

    public static EncounterHistory fromJSON(JSONObject obj) throws JSONException, ParseException
    {
        return build(ParseObjectId.Of(obj.getString(FIELD_PAIR_ID)),
                     extractEncounters(obj));
    }



    @Override
    public float getCompetency()
    {
        return (float) encounters.stream()
                                 .mapToDouble(this::encounterValue)
                                 .average()
                                 .orElse(0f);
    }

    @Override
    protected void sortEncounters()
    {
        // No need to sort the encounters, so overriding this method to do nothing.
    }
}
