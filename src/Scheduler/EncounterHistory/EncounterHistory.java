package Scheduler.EncounterHistory;

import Interfaces.JSONable;
import Objects.ParseObjectId;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 */
public abstract class EncounterHistory implements JSONable
{
    // JSON field strings
    protected static final String FIELD_PAIR_ID = "pairId";
    protected static final String FIELD_ENCOUNTERS = "encounters";
    protected static final String FIELD_COMPETENCY = "competency";
    protected static final String FIELD_NUM_ENCOUNTERS = "numEncounters";

    public final ParseObjectId pairId;
    protected final List<LearnEncounter> encounters;

    protected EncounterHistory(ParseObjectId pairId, Collection<LearnEncounter> encounters)
    {
        this.pairId = pairId;
        this.encounters = encounters != null
                          ? new ArrayList<>(encounters)
                          : new ArrayList<>();
        sortEncounters();
    }


    /**
     * Adds the given encounter to the encounter history.
     *
     * @param encounter The encounter to add.
     */
    public void addEncounter(LearnEncounter encounter)
    {
        encounters.add(encounter);
        sortEncounters();
    }

    /**
     * Adds the given encounters to the encounter history.
     *
     * @param encounters The encounters to add.
     */
    public void addEncounters(Collection<LearnEncounter> encounters)
    {
        this.encounters.addAll(encounters);
        sortEncounters();
    }

    /**
     * Adds all encounters in the given encounter history to this encounter history.
     *
     * @param history The encounter history to merge.
     */
    public void mergeHistory(EncounterHistory history)
    {
        this.encounters.addAll(history.encounters);
        sortEncounters();
    }

    /**
     * Utility method that sorts all encounters chronologically.
     */
    protected void sortEncounters()
    {
        Collections.sort(encounters, (o1, o2) -> o1.timestamp.compareTo(o2.timestamp));
    }

    /**
     * Static method for extracting the learning encounters from a JSON object.
     * This method simply serves to minimize duplicate code in subclasses.
     *
     * @param obj The JSON object to extract encounters from.
     * @return A collection of all extracted encounters.
     * @throws JSONException
     * @throws ParseException
     */
    protected static Collection<LearnEncounter> extractEncounters(JSONObject obj) throws JSONException, ParseException
    {
        Collection<LearnEncounter> encounters = new ArrayList<>();

        JSONArray arr = obj.getJSONArray(FIELD_ENCOUNTERS);
        for (int i = 0; i < arr.length(); i++)
            encounters.add(LearnEncounter.fromJSON(arr.getJSONObject(i)));

        return encounters;
    }

    /**
     * Returns the number of encounters in the encounter history.  This may simply
     * return encounters.size() for subclasses that use a full encounter history,
     * or some cached value for subclasses that use an encounter summary.
     */
    protected int getNumOfEncounters()
    {
        return encounters.size();
    }

    /**
     * Constructs a new JSON object to represent this encounter history, filling in the appropriate JSON fields.
     *
     * @return A JSON object that represents this encounter history.
     * @throws JSONException
     */
    @Override
    public JSONObject toJSON() throws JSONException
    {
        JSONObject obj = new JSONObject();

        // Store "pairId" field
        obj.put(FIELD_PAIR_ID, pairId.id);

        // Create JSON array of encounters
        JSONArray arr = new JSONArray();
        for (LearnEncounter enc : encounters)
            arr.put(enc.toJSON());

        // Store "encounters" field
        obj.put(FIELD_ENCOUNTERS, arr);

        // Store "competency" field
        obj.put(FIELD_COMPETENCY, getCompetency());

        // Store "numEncounters" field
        obj.put(FIELD_NUM_ENCOUNTERS, getNumOfEncounters());

        return obj;
    }


    // ABSTRACT METHODS
    /**
     * Gets an evaluation of the user's competency with a pairId based on their
     * encounter history.
     *
     * @return A value between 0 and 1, inclusive, that represents the competency level of the user for this pairId.
     */
    public abstract float getCompetency();

    /**
     * Calculates an individual competency value for a single encounter.
     *
     * @param encounter The encounter to evaluate for.
     * @return A value between 0 and 1, inclusive, that represents the user's
     * competency with the pairId in the given encounter.
     */
    protected abstract float encounterValue(LearnEncounter encounter);

}
