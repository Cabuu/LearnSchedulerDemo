package Scheduler.EncounterHistory;

import Objects.ParseObjectId;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class HistoryCollection
{

    private final Map<ParseObjectId, EncounterHistory> pairs = new HashMap<>();
    private final Function<ParseObjectId, EncounterHistory> emptyBuilder;

    private HistoryCollection(Function<ParseObjectId, EncounterHistory> emptyBuilder)
    {
        this.emptyBuilder = emptyBuilder;
    }

    public static HistoryCollection build(Function<ParseObjectId, EncounterHistory> emptyBuilder)
    {
        return new HistoryCollection(emptyBuilder);
    }

    public static HistoryCollection fromJSON(Function<ParseObjectId, EncounterHistory> emptyBuilder,
                                             Function<JSONObject, EncounterHistory> historyParser,
                                             JSONObject obj) throws JSONException
    {
        HistoryCollection hist = build(emptyBuilder);

        for (int i = 0; i < obj.names().length(); i++)
        {
            String field = obj.names().getString(i);

            JSONObject ehObj = obj.getJSONObject(field);

            EncounterHistory eh = historyParser.apply(ehObj);

            hist.mergeHistory(eh);
        }

        return hist;
    }

    public void addTranslationPair(ParseObjectId pairId)
    {
        if (pairs.containsKey(pairId))
            return;

        pairs.put(pairId, emptyBuilder.apply(pairId));
    }

    public void removeTranslationPair(ParseObjectId pairId)
    {
        pairs.remove(pairId);
    }

    public void mergeHistory(EncounterHistory history)
    {
        if (!pairs.containsKey(history.pairId))
            addTranslationPair(history.pairId);

        pairs.get(history.pairId).mergeHistory(history);
    }

    public void mergeCollection(HistoryCollection coll)
    {
        coll.getAllEncounterHistories().forEach(this::mergeHistory);
    }

    public void addEncounter(LearnEncounter encounter)
    {
        if (!pairs.containsKey(encounter.pairId))
            addTranslationPair(encounter.pairId);

        pairs.get(encounter.pairId).addEncounter(encounter);
    }

    public void addEncounters(Collection<LearnEncounter> encounters)
    {
        encounters.forEach(this::addEncounter);
    }

    public boolean hasTranslationPair(ParseObjectId id)
    {
        return pairs.containsKey(id);
    }

    public EncounterHistory getHistory(ParseObjectId id)
    {
        return hasTranslationPair(id) ? pairs.get(id) : null;
    }

    public void wipeHistory()
    {
        pairs.clear();
    }


    public Collection<EncounterHistory> getAllEncounterHistories()
    {
        return pairs.values();
    }

    public Collection<ParseObjectId> getAllTranslationPairIds()
    {
        return pairs.keySet();
    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject history = new JSONObject();
        for (EncounterHistory eh : pairs.values())
            history.put(eh.pairId.id, eh.toJSON());

        return history;
    }

}
