package Scheduler.EncounterHistory;

import Objects.ParseObjectId;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

/**
 * A completely dumbed down version of an encounter history that actually stores absolutely nothing.
 */
public class DumbEncounterHistory extends EncounterHistory
{

    protected DumbEncounterHistory(ParseObjectId pair, Collection<LearnEncounter> encounters)
    {
        super(pair, encounters);
    }

    public static DumbEncounterHistory build(ParseObjectId pair)
    {
        return new DumbEncounterHistory(pair, null);
    }

    public static DumbEncounterHistory fromJSON(JSONObject obj) throws JSONException
    {
        return build(ParseObjectId.Of(obj.getString(FIELD_PAIR_ID)));
    }


    @Override
    public void addEncounter(LearnEncounter encounter)
    {
        // DO NOTHING
    }

    @Override
    public void addEncounters(Collection<LearnEncounter> encounters)
    {
        // DO NOTHING
    }

    @Override
    public void mergeHistory(EncounterHistory history)
    {
        // DO NOTHING
    }

    @Override
    protected void sortEncounters()
    {
        // DO NOTHING
    }

    @Override
    protected int getNumOfEncounters()
    {
        return 0;
    }


    @Override
    public float getCompetency()
    {
        return 0f;
    }

    @Override
    protected float encounterValue(LearnEncounter encounter)
    {
        return 0f;
    }
}
