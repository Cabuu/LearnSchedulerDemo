package Scheduler.EncounterHistory;

import Objects.ParseObjectId;

import java.util.Collection;

/**
 */
public abstract class SimpleEncounterHistory extends EncounterHistory
{
    protected SimpleEncounterHistory(ParseObjectId pair, Collection<LearnEncounter> encounters)
    {
        super(pair, encounters);
    }

    @Override
    protected float encounterValue(LearnEncounter encounter)
    {
        switch (encounter.result)
        {
            case CORRECT:
                return 1f;
            case INCORRECT:
            case SKIPPED:
            default:
                return 0f;
        }
    }
}
