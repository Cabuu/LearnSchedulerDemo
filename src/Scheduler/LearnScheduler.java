package Scheduler;

import Interfaces.JSONable;
import Objects.LearnList;
import Objects.UserSettings;
import Objects.ParseObjectId;
import Scheduler.LearnEvent.LearnEvent;
import Scheduler.LearnProfile.LearnProfile;
import Utils.Vals;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.stream.Collectors;

/**
 */
public abstract class LearnScheduler implements JSONable
{
    protected static final String FIELD_SETTINGS = "settings";
    protected static final String FIELD_PROFILE_ID = "profileId";
    protected static final String FIELD_LIST_ID = "listId";
    protected static final String FIELD_DUE = "dueDate";
    protected static final String FIELD_START = "startDate";
    protected static final String FIELD_EVENTS = "events";

    protected final LearnProfile profile;
    protected final LearnList list;
    public Date dueDate;
    public Date startDate;
    protected final UserSettings settings;
    protected final Map<Integer, LearnEvent> eventMap;

    // This is here to cache the sorted order of the events, while maintaining the above map for O(1) event access.
    // This is a list of event IDs rather than the events themselves so that entries in the map can be entirely
    // replaced without needing to also update this list.
    protected final List<Integer> eventOrder;

    protected LearnScheduler(UserSettings settings, LearnProfile profile, LearnList list, Date startDate, Date dueDate)
    {
        this.profile = profile;
        this.list = list;
        this.settings = settings;
        this.dueDate = dueDate;
        this.startDate = startDate;
        this.eventMap = new HashMap<>();
        this.eventOrder = new ArrayList<>();
    }

    @Override
    public JSONObject toJSON() throws JSONException
    {
        JSONObject obj = new JSONObject();

        obj.put(FIELD_SETTINGS, settings.toJSON());
        obj.put(FIELD_PROFILE_ID, profile.objectId);
        obj.put(FIELD_LIST_ID, list.objectId);
        obj.put(FIELD_DUE, Vals.ISO_DATE.format(dueDate));
        obj.put(FIELD_START, Vals.ISO_DATE.format(startDate));

        JSONArray arr = new JSONArray();
        for (Integer id : eventOrder)
            arr.put(eventMap.get(id).toJSON());

        obj.put(FIELD_EVENTS, arr);

        return obj;
    }

    /**
     * Retrieves the full schedule from this scheduler.  All events are returned in chronological order.
     *
     * @return A chronologically-sorted list of all scheduled learn events.
     */
    public List<LearnEvent> getSchedule()
    {
        return eventOrder.stream().map(eventMap::get).collect(Collectors.toList());
    }

    /**
     * Retrieves the next learning event in the schedule in relation to the current time.
     *
     * @return The next future learning event.  If there are no future events, then returns null.
     */
    public LearnEvent getNextEvent()
    {
        return getEventAfter(new Date());
    }

    /**
     * Retrieves the next learning event in the schedule in relation to the given time.
     *
     * @return The next future learning event after the given date.  If there are no future events, then returns null.
     */
    public LearnEvent getEventAfter(Date date)
    {
        for (int eventId : eventOrder)
        {
            if (date.before(eventMap.get(eventId).date))
                return eventMap.get(eventId);
        }
        return null;
    }

    /**
     * Retrieves a list of events that occur on the same day as the given date.  The events returned will be sorted
     * chronologically.
     * ** FOR CURRENT IMPLEMENTATION, THIS METHOD RETURNS A MAXIMUM OF ONE EVENT... BUT THIS SIGNATURE ALLOWS FOR THE
     *    POSSIBILITY OF HAVING MORE.
     *
     * @param date The date to get events for.
     * @return A chronologically-sorted list of events for the given date.
     */
    public List<LearnEvent> getEventsOnDate(Date date)
    {
        return eventOrder.stream()
                         .filter(eventId -> daysBetween(date, eventMap.get(eventId).date) == 0)
                         .map(eventMap::get)
                         .collect(Collectors.toList());
    }

    /**
     * Retrieves the first learning event to occur on the given date.
     *
     * @param date The date to get events for.
     * @return The first learning event on the given date.  If there are no events on the given date, returns null.
     */
    public LearnEvent getFirstEventOnDate(Date date)
    {
        for (int eventId : eventOrder)
        {
            if (daysBetween(date, eventMap.get(eventId).date) == 0)
                return eventMap.get(eventId);
        }
        return null;
    }

    /**
     * Retrieves the number of learning days in the schedule.
     */
    public int getDaysInSchedule()
    {
        return daysBetween(startDate, dueDate) + 1;
    }

    /**
     * Utility method for sorting all events chronologically.
     */
    protected void sortEvents()
    {
        Collections.sort(eventOrder, (e1, e2) -> eventMap.get(e1).date.compareTo(eventMap.get(e2).date));
    }

    /**
     * Adds the given event to the schedule if it is not already present.  Otherwise, does nothing.
     * @param event The event to add to the schedule.
     */
    protected void addEvent(LearnEvent event)
    {
        if (eventMap.containsKey(event.id))
            return;

        eventMap.put(event.id, event);

        eventOrder.add(binarySearchDate(event.date), event.id);
    }

    /**
     * Adds the given events to the schedule if they is not already present.  Otherwise, does nothing.
     * @param event The events to add to the schedule.
     */
    protected void addEvents(Collection<LearnEvent> event)
    {
        event.forEach(this::addEvent);
    }

    /**
     * Calculates the difference in days between the two given dates.
     * @param d1 The first (earlier) date to compare
     * @param d2 The second (later) date to compare
     * @return The number of days from d1 to d2.
     */
    protected int daysBetween(Date d1, Date d2)
    {
        int msPerDay = 1000 * 60 * 60 * 24;

        long t1 = d1.getTime() - (d1.getTime() % msPerDay);
        long t2 = d2.getTime() - (d2.getTime() % msPerDay);

        return (int)(t2 - t1) / msPerDay;
    }

    /**
     * Performs a binary search of the events list to identify the index of the first event that falls after
     * the given date.
     *
     * @param date The date to search for in the events list.
     * @return The index of the first event that falls after the given date.
     */
    private int binarySearchDate(Date date)
    {
        int left = 0;
        int right = eventOrder.size() - 1;

        while (left < right)
        {
            int middle = (int)Math.floor((double)(left + right) / 2);

            int comparison = eventMap.get(eventOrder.get(middle)).date.compareTo(date);
            if (comparison < 0)
                left = middle + 1;
            else if (comparison > 0)
                right = middle - 1;
            else
                return middle;
        }

        return right + 1;
    }

    /*
     * ABSTRACT METHODS
     */

    /**
     * Updates an event, replacing the event in the LearnEvent collection
     * with the matching unique identifier.  This triggers a reevaluation
     * of the entire schedule, resulting in future events being potentially
     * changed.  This should be called whenever an event has been completed
     * (or skipped) by a user.
     *
     * @param event The learning event to update.
     */
    public abstract void updateEvent(LearnEvent event);

    /**
     * Produces the full schedule of learning events, including the date they are to take place on and the pairs
     * they are to present.
     *
     * @return Returns this learning scheduler in order to facilitate chaining.
     */
    protected abstract LearnScheduler generateSchedule();

    protected abstract LearnEvent getNewLearnEvent(Enums.EventType type, Date date, Collection<ParseObjectId> pairs);

}
