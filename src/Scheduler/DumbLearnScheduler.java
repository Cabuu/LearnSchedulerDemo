package Scheduler;

import Objects.LearnList;
import Objects.ParseObjectId;
import Objects.UserSettings;
import Scheduler.LearnEvent.DumbLearnEvent;
import Scheduler.LearnEvent.LearnEvent;
import Scheduler.LearnProfile.LearnProfile;
import Utils.Vals;
import com.sun.deploy.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * A very simple implementation of the learn scheduler.  Uses dumbed down versions of EncounterHistory, LearnEvent,
 * and LearnProfile that do not record any encounter information.  As a result, this scheduler does not take into
 * account any learning encounters when updating events (and in fact, does not change at all with event updates).
 */
public class DumbLearnScheduler extends LearnScheduler
{
    private static final int DEFAULT_WORDS_PER_DAY = 8;
    private static final int DEFAULT_TEST_DAYS = 2;

    private static final Function<Integer, Integer> RECOMMENDED_DAYS =
            (words) -> DEFAULT_TEST_DAYS + (int) Math.ceil((float) words / DEFAULT_WORDS_PER_DAY);

    protected DumbLearnScheduler(UserSettings settings, LearnProfile profile, LearnList list,
                                 Date startDate, Date dueDate)
    {
        super(settings, profile, list, startDate, dueDate);
    }

    public static LearnScheduler build(UserSettings settings, LearnProfile profile, LearnList list, Date due)
    {
        return new DumbLearnScheduler(settings, profile, list, new Date(), due).generateSchedule();
    }

    public static LearnScheduler fromJSON(LearnProfile profile, JSONObject obj) throws JSONException, ParseException
    {
        LearnScheduler sched = new DumbLearnScheduler(
                UserSettings.fromJSON(obj.getJSONObject(FIELD_SETTINGS)),
                profile,
                LearnList.buildFromId(ParseObjectId.Of(obj.getString(FIELD_PROFILE_ID))),
                Vals.ISO_DATE.parse(obj.getString(FIELD_START)),
                Vals.ISO_DATE.parse(obj.getString(FIELD_DUE)));

        JSONArray arr = obj.getJSONArray(FIELD_EVENTS);
        for (int i = 0; i < arr.length(); i++)
            sched.addEvent(DumbLearnEvent.fromJSON(profile, arr.getJSONObject(i)));

        return sched;
    }

    /**
     * Updates the given event in the schedule's event list.  If the event does not exist yet in the scheduler,
     * then the event is added as a new event.
     *
     * @param event The learning event to update.
     */
    @Override
    public void updateEvent(LearnEvent event)
    {
        if (!eventMap.containsKey(event.id))
            addEvent(event);
        else
            eventMap.put(event.id, event);

        sortEvents();
    }

    /**
     * Produces the full schedule of learning events, including the date they are to take place on and the pairs
     * they are to present.
     *
     * @return Returns this learning scheduler in order to facilitate chaining.
     */
    @Override
    protected LearnScheduler generateSchedule()
    {
        // Get today's date
        Date today = new Date();

        // Store the total pairId count
        int pairCount = list.pairs.size();
        // Calculate the number of days available for studying (including today AND dueDate date)
        int days = daysBetween(today, dueDate) + 1;

        // If the dueDate date is today (or in the past?), add a big study and test session for today, then be done.
        if (days <= 1)
        {
            addEvents(studyAndTest(today, list.pairs, list.pairs));

            sortEvents();

            return this;
        }

        // Calculate the recommended number of days for the number of pairs being learned.
        int recommendedDays = RECOMMENDED_DAYS.apply(pairCount);


        // ** OUTPUT FOR TESTING
        System.out.println(String.format("Generating schedule for (%d) pairs over (%d) days.  Recommended days for " +
                                         "this amount of pairs:  %d",
                                         pairCount, days, recommendedDays));
        System.out.println("=====================\n");


        // Find the number of appropriate exam days... if there are fewer days to learn than the default number
        // of exam days, have only one test.  Otherwise, have the default number of exams.
        int examDays = days <= DEFAULT_TEST_DAYS
                       ? 1
                       : DEFAULT_TEST_DAYS;

        // Find the average number of pairs per day to learn.  If we have to learn at an accelerated rate,
        // increase the pairs-per-day over the default, otherwise use the default.
        int wordsPerDay = days <= recommendedDays
                          ? (int) Math.ceil((float) pairCount / (days - examDays))
                          : DEFAULT_WORDS_PER_DAY;

        // Find the fractional interval between days to add to the schedule.  If the schedule duration is less than or
        // equal to the recommended, this should equal exactly one.  Otherwise, this should be some value > 1.
        // Example:  dayInterval = 1.5, this means that there should be 2 study sessions every 3 days.
        // Example:  dayInterval = 3, this means that there should be 1 study session every 3 days.
        float dayInterval = (days - examDays) / ((float) Math.ceil((float) pairCount / wordsPerDay));

        // List for temporarily holding onto the study sessions generated, used for referring to when determining
        // what pairs to test in testing events.
        List<LearnEvent> studyEvents = new ArrayList<>();

        // Boolean for tracking when a particular day should be a study day and when not.
        boolean shouldStudy = true;

        // Iterate through the full length of the schedule, incrementing by the dayInterval.
        for (float dayOffset = 0f; dayOffset < days; dayOffset += dayInterval)
        {
            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            cal.add(Calendar.DATE, (int) dayOffset);  // day offset rounded down to nearest integer

            // ** OUTPUT FOR TESTING
            System.out.println("DAY " + (int) (dayOffset + 1) + ":\n-------------");

            // See if the current day offset is appropriate for creating an exam event.  If so, an exam event is created
            // and added to the event list, and this iteration of the loop is done (going to the next day offset).
            if (addExam(days, examDays, dayOffset, dayInterval, cal.getTime()))
            {
                // ** OUTPUT FOR TESTING
                System.out.println(String.format("Exam added (%d pairs).\n",
                                                 eventMap.get(eventOrder.get(eventOrder.size() - 1)).getPairs().size()));
                // Ensure the next event is a study event
                // ** ACTUALLY NOT **  shouldStudy = true;
                continue;
            }

            if (shouldStudy)
            {
                // Get the list of pairs to study by pulling a sublist out of the complete pairId list
                // Grab an amount of pairs equal to double the pairs-per-day
                List<ParseObjectId> studyPairs = list.pairs.subList(Math.min(studyEvents.size() * wordsPerDay * 2,
                                                                               list.pairs.size()),
                                                                      Math.min((studyEvents.size() + 1) * wordsPerDay * 2,
                                                                               list.pairs.size()));

                // Generate the study event
                LearnEvent event = getNewLearnEvent(Enums.EventType.LEARN, cal.getTime(), studyPairs);
                // Add the study event to the study events list
                studyEvents.add(event);
                // Add the event to the master list of events
                addEvent(event);

                // Ensure the next event is not a study event
                shouldStudy = false;


                // ** OUTPUT FOR TESTING
                System.out.println(String.format("Study session [%d] added (%d pairs).  Words: [%s]",
                                                 event.id,
                                                 studyPairs.size(),
                                                 StringUtils.join(
                                                         studyPairs.stream()
                                                                   .map(w -> w.id)
                                                                   .sorted()
                                                                   .collect(Collectors.toList()),
                                                         " ")));
                System.out.println();
            }
            else
            {

                List<ParseObjectId> testPairs = new ArrayList<>();
                // This incrementer is doubled with each pass of the loop, resulting in an increasing interval between
                // study sessions where testing pairs are taken from.
                // Where N = (the number of study events), the study events chosen will be:
                // N, N - 1, N - 3, N - 7, N - 15, etc.
                int inc = 1;
                for (int i = studyEvents.size() - 1; i >= 0; i -= inc, inc *= 2)
                {
                    // Add the pairs from the selected study session to this testing session
                    testPairs.addAll(studyEvents.get(i).getPairs());
                }

                // Generate the test event add it to the master list of events
                LearnEvent event = getNewLearnEvent(Enums.EventType.TEST, cal.getTime(), testPairs);
                addEvent(event);


                // Ensure the next non-exam event is a study event
                shouldStudy = true;


                // ** OUTPUT FOR TESTING
                System.out.println(String.format("Test session [%d] added (%d pairs).  Words: [%s]",
                                                 event.id,
                                                 testPairs.size(),
                                                 StringUtils.join(testPairs.stream()
                                                                           .map(w -> w.id)
                                                                           .sorted()
                                                                           .collect(Collectors.toList()),
                                                                  " ")));
                System.out.println();
            }
        }


        // Return this LearnScheduler object to facilitate chaining
        return this;
    }

    /**
     * Tests if adding an exam to the schedule is appropriate for the given day offset.  If the day is determined to
     * be an exam day, a test event is added to the schedule with all pairs thus far learned, and this method returns
     * true.  Otherwise, no events are added and this method returns false.
     *
     * @param days        Total number of days to schedule for
     * @param examDays    The number of test days to schedule
     * @param dayOffset   The current day offset, representing the day to schedule events for
     * @param dayInterval The amount that schedule days are incremented by
     * @param date        The date to set as the event date, if an event is scheduled.
     * @return Returns true if a test event was scheduled, false otherwise.
     */
    private boolean addExam(int days, int examDays, float dayOffset, float dayInterval, Date date)
    {
        // Iterate through all exam days
        for (int i = 1; i <= examDays; i++)
        {
            // Calculate the appropriate day offset for this exam
            float examOffset = i * ((float) (days - (examDays - i) - 1) / examDays);

            // If the exam's offset is in the future compared to the given day offset, break the loop as there
            // is no longer any reason to search.
            if (examOffset > dayOffset)
                break;

            // If the exam's offset is in the past compared to the given day offset, but in the future compared
            // to the previous day offset, then this day must be an exam day.  Add an exam event and return true.
            if (examOffset > dayOffset - dayInterval)
            {
                addEvent(getNewLearnEvent(Enums.EventType.EXAM,
                                          date,
                                          collectPairs(eventMap.values())));
                return true;
            }
        }
        // If the given day was not suitable for an exam, then return false.
        return false;
    }

    /**
     * Generates a study event and a test event both on the same given date, using their own pairId lists as given.
     * The study event is scheduled 1 minute prior to the testing event just to ensure appropriate ordering of
     * events.
     *
     * @param date       The date to schedule both the studying and the testing events on.
     * @param studyPairs The pairs to introduce in the study session.
     * @param testPairs  The pairs to review in the testing session.
     * @return A list containing 2 learn events; the first one being a study event, and the second a testing event.
     */
    private List<LearnEvent> studyAndTest(Date date, Collection<ParseObjectId> studyPairs, Collection<ParseObjectId> testPairs)
    {
        List<LearnEvent> events = new ArrayList<>();

        // Hours and minutes are irrelevant for scheduling; only day matters.  Minutes do help ordering of events
        // within the day, however.
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 58);

        // Generate the study event
        events.add(getNewLearnEvent(Enums.EventType.LEARN, cal.getTime(), studyPairs));

        cal.set(Calendar.MINUTE, 59);

        // Generate the testing event
        events.add(getNewLearnEvent(Enums.EventType.TEST, cal.getTime(), testPairs));

        // Return both created events
        return events;
    }

    /**
     * Creates a collection of TranslationPair objects that contain all pairs in all of the given learning events.  There are
     * guaranteed to be no duplicates of any pairs.
     *
     * @param events The events to collect pairs from.
     * @return All pairs in all of the given events, with duplicates ignored.
     */
    private Collection<ParseObjectId> collectPairs(Collection<LearnEvent> events)
    {
        Collection<ParseObjectId> pairs = new HashSet<>();
        events.forEach(e -> pairs.addAll(e.getPairs()));
        return pairs;
    }

    @Override
    protected LearnEvent getNewLearnEvent(Enums.EventType type, Date date, Collection<ParseObjectId> pairs)
    {
        return DumbLearnEvent.buildNew(type, date, profile, pairs);
    }
}
