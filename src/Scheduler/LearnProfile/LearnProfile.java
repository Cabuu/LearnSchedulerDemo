package Scheduler.LearnProfile;

import Interfaces.JSONable;
import Objects.ParseObjectId;
import Scheduler.EncounterHistory.EncounterHistory;
import Scheduler.EncounterHistory.LearnEncounter;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 */
public abstract class LearnProfile implements JSONable
{
    protected static final String FIELD_OBJECT_ID = "objectId";
    protected static final String FIELD_USER_ID = "userId";
    protected static final String FIELD_SOURCE_LANG = "sourceLang";
    protected static final String FIELD_TARGET_LANG = "targetLang";
    protected static final String FIELD_INFO = "info";
    protected static final String FIELD_INFO_HISTORY = "history";
    protected static final String FIELD_INFO_COMPETENCY = "competency";

    public final ParseObjectId objectId;
    public final ParseObjectId userId;
    public final String sourceLang;
    public final String targetLang;
    protected final Map<ParseObjectId, EncounterHistory> pairs;

    protected LearnProfile(ParseObjectId objectId, ParseObjectId userId, String sourceLang, String targetLang)
    {
        this.objectId = objectId;
        this.userId = userId;
        this.sourceLang = sourceLang;
        this.targetLang = targetLang;
        pairs = new HashMap<>();
    }

    @Override
    public JSONObject toJSON() throws JSONException
    {
        JSONObject obj = new JSONObject();

        obj.put(FIELD_OBJECT_ID, objectId);
        obj.put(FIELD_USER_ID, userId);
        obj.put(FIELD_SOURCE_LANG, sourceLang);
        obj.put(FIELD_TARGET_LANG, targetLang);

        JSONObject info = new JSONObject();

        JSONObject history = new JSONObject();
        for (EncounterHistory entry : pairs.values())
            history.put(entry.pairId.id, entry.toJSON());
        info.put(FIELD_INFO_HISTORY, history);
        info.put(FIELD_INFO_COMPETENCY, evaluateCompetency());

        obj.put(FIELD_INFO, info);

        return obj;
    }

    public EncounterHistory getHistory(ParseObjectId pair)
    {
        return pairs.get(pair);
    }

    public void addEncounter(LearnEncounter encounter)
    {
        if (pairs.containsKey(encounter.pairId))
            pairs.get(encounter.pairId).addEncounter(encounter);
        else
        {
            EncounterHistory history = getNewEncounterHistory(encounter.pairId);
            history.addEncounter(encounter);
            pairs.put(encounter.pairId, history);
        }
    }

    public void addEncounters(Collection<LearnEncounter> encounters)
    {
        encounters.forEach(this::addEncounter);
    }

    public void addHistory(EncounterHistory history)
    {
        if (pairs.containsKey(history.pairId))
            pairs.get(history.pairId).mergeHistory(history);
        else
            pairs.put(history.pairId, history);
    }

    public void addTranslationPair(ParseObjectId pair)
    {
        if (pairs.containsKey(pair))
            return;

        pairs.put(pair, getNewEncounterHistory(pair));
    }

    /**
     * Construct an empty encounter history.  The type of encounter history is to be determined by
     * subclasses of this learning profile.
     *
     * @param pair The pairId to create the encounter history for.
     * @return The new empty encounter history.
     */
    protected abstract EncounterHistory getNewEncounterHistory(ParseObjectId pair);

    /**
     * Using all of the pairs in this learning profile (along with their
     * encounter histories), estimates a value of the user's competency
     * with the target language.
     *
     * @return A value between 0-100 (inclusive) that represents the user's
     *          competency with the target language.
     */
    public abstract float evaluateCompetency();
}

