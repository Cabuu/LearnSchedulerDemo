package Scheduler.LearnProfile;

import Objects.ParseObjectId;
import Scheduler.EncounterHistory.EncounterHistory;

/**
 */
public abstract class SimpleLearnProfile extends LearnProfile
{
    protected SimpleLearnProfile(ParseObjectId objectId, ParseObjectId userId, String sourceLang, String targetLang)
    {
        super(objectId, userId, sourceLang, targetLang);
    }

    @Override
    public float evaluateCompetency()
    {
        return (float) pairs.values()
                            .stream()
                            .mapToDouble(EncounterHistory::getCompetency)
                            .average()
                            .orElse(0f);
    }
}
