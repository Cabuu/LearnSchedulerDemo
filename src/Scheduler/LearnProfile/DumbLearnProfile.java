package Scheduler.LearnProfile;

import Objects.ParseObjectId;
import Scheduler.EncounterHistory.DumbEncounterHistory;
import Scheduler.EncounterHistory.EncounterHistory;
import Scheduler.EncounterHistory.LearnEncounter;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Collection;

/**
 * A completely dumbed down learning profile that uses the DumbEncounterHistory
 */
public class DumbLearnProfile extends LearnProfile
{
    protected DumbLearnProfile(ParseObjectId objectId, ParseObjectId userId, String sourceLang, String targetLang)
    {
        super(objectId, userId, sourceLang, targetLang);
    }

    public static DumbLearnProfile build(ParseObjectId objectId, ParseObjectId userId, String sourceLang, String targetLang)
    {
        return new DumbLearnProfile(objectId, userId, sourceLang, targetLang);
    }

    public static LearnProfile fromJSON(JSONObject obj) throws JSONException, ParseException
    {
        return build(ParseObjectId.Of(obj.getString(FIELD_OBJECT_ID)),
                     ParseObjectId.Of(obj.getString(FIELD_USER_ID)),
                     obj.getString(FIELD_SOURCE_LANG),
                     obj.getString(FIELD_TARGET_LANG));
    }

    @Override
    public EncounterHistory getHistory(ParseObjectId pair)
    {
        return getNewEncounterHistory(pair);
    }

    @Override
    public void addEncounter(LearnEncounter encounter)
    {
        // DO NOTHING
    }

    @Override
    public void addEncounters(Collection<LearnEncounter> encounters)
    {
        // DO NOTHING
    }

    @Override
    public void addHistory(EncounterHistory history)
    {
        // DO NOTHING
    }

    @Override
    public void addTranslationPair(ParseObjectId pair)
    {
        // DO NOTHING
    }

    @Override
    protected EncounterHistory getNewEncounterHistory(ParseObjectId pair)
    {
        return DumbEncounterHistory.build(pair);
    }

    @Override
    public float evaluateCompetency()
    {
        return 0f;
    }
}
