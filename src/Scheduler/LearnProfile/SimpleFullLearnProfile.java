package Scheduler.LearnProfile;

import Objects.ParseObjectId;
import Scheduler.EncounterHistory.EncounterHistory;
import Scheduler.EncounterHistory.SimpleFullEncounterHistory;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Iterator;

/**
 */
public class SimpleFullLearnProfile extends SimpleLearnProfile
{

    /**
     * Private constructor, use static constructor instead.
     */
    protected SimpleFullLearnProfile(ParseObjectId objectId, ParseObjectId userId, String sourceLang, String targetLang)
    {
        super(objectId, userId, sourceLang, targetLang);
    }

    /**
     * Static constructor, creates a new learn profile object and returns it.
     *
     * @return The newly constructed LearnProfile object.
     */
    public static LearnProfile build(ParseObjectId objectId, ParseObjectId userId, String sourceLang, String targetLang)
    {
        return new SimpleFullLearnProfile(objectId, userId, sourceLang, targetLang);
    }


    public static LearnProfile buildFromId(String objectId)
    {
        // This is where a learning profile would be constructed either by finding the learning list with the given
        // object ID in the local database or by querying for it remotely.
        return build(null, null, null, null);
    }

    public static LearnProfile fromJSON(JSONObject obj) throws JSONException, ParseException
    {
        LearnProfile profile = build(ParseObjectId.Of(obj.getString(FIELD_OBJECT_ID)),
                                     ParseObjectId.Of(obj.getString(FIELD_USER_ID)),
                                     obj.getString(FIELD_SOURCE_LANG),
                                     obj.getString(FIELD_TARGET_LANG));

        JSONObject history = obj.getJSONObject(FIELD_INFO).getJSONObject(FIELD_INFO_HISTORY);
        Iterator<?> it = history.keys();
        while (it.hasNext())
        {
            String key = (String)it.next();
            profile.addHistory(SimpleFullEncounterHistory.fromJSON(history.getJSONObject(key)));
        }

        return profile;
    }


    @Override
    protected EncounterHistory getNewEncounterHistory(ParseObjectId pair)
    {
        return SimpleFullEncounterHistory.build(pair, null);
    }
}
