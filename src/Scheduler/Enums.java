package Scheduler;

public abstract class Enums
{
    public enum EncounterType
    {
        Study_Anim,
        Study_Graphic,
        Study_Text,
        Study_Default,

        Test_MultipleChoice,
        Test_FillInTheBlank,
        Test_LetterSalad,
        Test_Pairing,
        Test_Listening
        // ETC ETC
    }

    public enum EventType
    {
        LEARN,
        TEST,
        EXAM,
        PAUSE,
        NONE
    }

    public enum EncounterResult
    {
        CORRECT,
        INCORRECT,
        SKIPPED,
        PARTIAL
    }

    public enum EventStatus
    {
        PENDING,
        INCOMPLETE,
        COMPLETE,
        MISSED
    }
}
